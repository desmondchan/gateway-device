#!/bin/bash
sudo chmod u+x bluetooth_service.conf
sudo chmod u+x wifi_service.conf
sudo chmod u+x webserver_service.conf
sudo chmod u+x mqtt_service.conf
sudo chmod u+x reset_service.conf

cd /etc/supervisor/conf.d/
cp /home/pi/raspberry-gateway/supervisor-config/bluetooth_service.conf .
cp /home/pi/raspberry-gateway/supervisor-config/wifi_service.conf .
cp /home/pi/raspberry-gateway/supervisor-config/webserver_service.conf .
cp /home/pi/raspberry-gateway/supervisor-config/mqtt_service.conf .
cp /home/pi/raspberry-gateway/supervisor-config/update_service.conf .
cp /home/pi/raspberry-gateway/supervisor-config/reset_service.conf .

supervisorctl reread
supervisorctl update