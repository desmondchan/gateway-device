import paho.mqtt.client as mqtt
from subprocess import Popen,PIPE, call
from datetime import datetime, timedelta
import queue
import threading
import time
import logging
import configparser
import connectionmanager
from time import sleep
import os
import gpsdata 
import webserver 

config = configparser.ConfigParser()
config_file = '/home/pi/raspberry-gateway/config/bewhere.conf'
nogps_cmd = '/home/pi/raspberry-gateway/scripts/./filter.sh'
gps_cmd = '/home/pi/raspberry-gateway/scripts/./gps-filter.sh'
logging.basicConfig(level=logging.DEBUG,format='%(asctime)s - %(levelname)s - %(message)s')

"""
Module Name: mqttmanager.py
Author: Desmond Chan
Purpose: This module will establish a connection to the MQTT server using the parameters
         defined in the bewhere.conf file and then send bluetooth data from filter.sh

This module uses a producer thread to queue the data read from filter.sh and then uses
another consumer thread to de-queue this data and then publish it to the MQTT server

"""

class deviceConfiguration:
    def __init__(self):
        try:
            self.cache = {}
            logging.debug("Reading Config File ...")
            file = open(config_file,'r')
            config.read(config_file)
            mqtt_settings = config['MQTT']
            self.host = mqtt_settings.get('host')
            self.port = mqtt_settings.get('port')
            self.username = mqtt_settings.get('username')
            self.password = mqtt_settings.get('password')
            self.topic = mqtt_settings.get('topic')

            misc_settings = config['Misc']
            self.interval = misc_settings.get('interval')
            self.gps = misc_settings.get('gps')
            file.close
            
            logging.debug("Configuration Set ...")
            self.client = mqtt.Client()
            self.client.on_connect = self.on_connect
            self.client.on_disconnect = self.on_disconnect
            self.client.on_message = self.on_message
            self.client.on_publish = self.on_publish
            self.client.username_pw_set(self.username,self.password)
            
        except Exception as e:
            print("Failed to initialize MQTT Client: {0}".format(e))

    def on_connect(self,client,userdata,flags,rc):
        logging.debug("Attempted Connection with result code {0}".format(str(rc)))
        #self.client.subscribe(self.topic,0)

    def on_message(self,client,userdata,msg):
        logging.debug("Message Topic: {0}".format(str(msg.payload)))

    def on_publish(self,client,userdata,mid):
        #logging.debug("mid: " + str(mid))
        pass

    def on_disconnect(self,client,userdata,rc):
        while rc != 0:
            sleep(2)
            logging.debug("Disconnected from server, attempting to reconnect...")
            rc = client.reconnect()

    def consumer(self,q):
        while(True):
            item = q.get()
            logging.debug(item)
            self.client.publish(self.topic, item,qos=0)
            log_path = '/home/pi/raspberry-gateway/logs/' + self.logTime()
            with open(log_path, 'a+') as log_file:
                log_file.write(item + '\n')
            q.task_done()


    def producer(self,q):
        # the main thread will put new items to the queue infinitely
        try:
            logging.debug("GPS Setting: {0}".format(self.gps))
            if self.gps == "True":
                gpsmonitor = gpsdata.gpsManager()
                gpsmonitor.threadStart()
                cmd = gps_cmd
            else:
                cmd = nogps_cmd
            with Popen(cmd,stdout=PIPE, bufsize=1, universal_newlines=True) as p:
                for line in p.stdout:
                    line = line.strip()
                    if self.gps == "True":
                        line = ','.join((line, str(self.epochtime()), str(gpsmonitor.latitude), str(gpsmonitor.longitude) ))
                    deviceData = line.split(',')
                    name = threading.currentThread().getName()
                    beaconID = deviceData[1]
                    btMACaddress = deviceData[2]
                    rssi = deviceData[3]
                    payload = self.convertPayload(deviceData[4])
                    output = ','.join(deviceData)
                    if self.checkData(beaconID,output):
                        q.put(output)        
                        
            q.join()
        except OSError as e:
            print("Error Connecting:")

    def currentTime(self):
        curtime = datetime.now()
        return curtime.strftime('%Y-%m-%d %H:%M:%S')

    def logTime(self):
        logtime = datetime.now()
        return logtime.strftime('%Y-%m-%d')

    def epochtime(self):
        return int(time.time() * 1000)

    #This method converts the payload into tuples for easier readablity if needed
    def convertPayload(self,data):
        line = data
        n = 2
        return [line[i:i+n] for i in range(0,len(line),n)]

    #checks the time between the message and will return true if the message is overdue
    def intervalCheck(self,beaconID,difference,interval):
        lastsent = difference - int(interval)
        if lastsent >= 0:
            return True
        else:
            return False

    def checkData(self,beaconID,output):
        start_time = time.time()
        if self.cache.get(beaconID):
            prev_time = datetime.strptime(self.cache[beaconID][0],'%Y-%m-%d %H:%M:%S')
            cur_time = datetime.strptime(self.currentTime(), '%Y-%m-%d %H:%M:%S')
            difference = cur_time - prev_time
            if self.intervalCheck(beaconID,difference.seconds,self.interval):
                self.cache[beaconID] = [self.currentTime(),output]
                return True
            else:
                return False
        else:
            #if beacon is not in the server cache add and return TRUE so send right away
            self.cache[beaconID] = [self.currentTime(),output]
            return True

    def createConnection(self):
        try:
            logging.debug("Creating Connection ...")

            self.client.connect(host=self.host,port=int(self.port)) 
            q = queue.Queue()
    
            consumer = threading.Thread(name = "Consumer Thread", target=self.consumer, args=(q,))
            consumer.start()

            producer = threading.Thread(name = "Producer Thread", target=self.producer, args=(q,))
            producer.start()

            q.join()
            self.client.loop_forever()
        except (ConnectionRefusedError,OSError) as e:
             logging.debug("Connection to Server Failed ... Error: {0}".format(e))
             logging.debug("Retrying Connection ...")
             main()
            

    
  


def main():
    try:
        if connectionmanager.main() is True:
            bewhereconnect = deviceConfiguration()
            sleep(1)
            bewhereconnect.createConnection()
        else:
            logging.debug("connection is not set ...")
            logging.debug("No WiFi Scheme Found ... running web server ...")
            webserver.main()
    except Exception as e:
        logging.debug("Error in main(): {0}".format(e))
    
if __name__ == "__main__":
    main()
 
