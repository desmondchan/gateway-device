import os
from threading import Thread
from gps3 import agps3



class gpsManager(object):

    def __init__(self):
        self._latitude = 0
        self._longitude = 0

    @property
    def latitude(self):
        return self._latitude
    @property
    def longitude(self):
        return self._longitude

    @latitude.setter
    def latitude(self,value):
        self._latitude = value

    @longitude.setter
    def longitude(self,value):
        self._longitude = value

    def monitor(self):
        gps_socket = agps3.GPSDSocket()
        data_stream = agps3.DataStream()
        gps_socket.connect()
        gps_socket.watch()
        for new_data in gps_socket:
            if new_data:
                data_stream.unpack(new_data)
                self._latitude = data_stream.lat
                self._longitude = data_stream.lon
    
    def threadStart(self):
        background_task = Thread(name = "Monitor Thread", target=self.monitor)
        background_task.start()

def printdata():
    print("Printing Data......")
    gps_socket = agps3.GPSDSocket()
    data_stream = agps3.DataStream()
    gps_socket.connect()
    gps_socket.watch()
    for new_data in gps_socket:
            if new_data:
                data_stream.unpack(new_data)
                print("Latitude: {0}".format(data_stream.lat))
                print("Longitude: {0}".format(data_stream.lon))


if __name__ == "__main__":
    printdata()

    