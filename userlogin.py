import uuid
import hashlib
import flask_login
import configparser

config = configparser.SafeConfigParser(allow_no_value=True, empty_lines_in_values=False)
config_file = '/home/pi/raspberry-gateway/config/webserver.conf'

#Development Path
#config_file = '/Users/development/orange-gateway/config/webserver.conf'

class User(flask_login.UserMixin):
    pass

def get_password():
    config.read(config_file)
    password = config['User']['password']
    return password

def hash_password(password):
    salt = uuid.uuid4().hex
    return hashlib.md5(salt.encode() + password.encode()).hexdigest() + ':' + salt

def validate_password(hashed_password, user_password):
    password, salt = hashed_password.split(':')
    return password == hashlib.md5(salt.encode() + user_password.encode()).hexdigest()


def test_hash():
    test_password = hash_password('password')
    print("Hashed Password: {0}".format(test_password))
    #print("Testing get_password: {0}".format(get_password()))
    print("Value:{0}".format(validate_password(get_password(),'password')))


if __name__ == '__main__': test_hash()
