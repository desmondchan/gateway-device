import paho.mqtt.client as mqtt
from subprocess import Popen,PIPE, call
from datetime import datetime, timedelta
import queue
import threading
import time
import logging
import configparser
from time import sleep
config = configparser.ConfigParser()

logging.basicConfig(level=logging.DEBUG,format='%(asctime)s - %(levelname)s - %(message)s')

class deviceConfiguration:
    def __init__(self):
        try:
            self.cache = {}
            print("Reading File")
            file=open('/home/pi/raspberry-gateway/database/bewhere.conf','r')
            config.read('/home/pi/raspberry-gateway/database/bewhere.conf')
            settings = config['Wifi']
            mqtt_settings = config['MQTT']
            self.interval = settings.get('interval')
            self.host = mqtt_settings.get('host')
            self.port = mqtt_settings.get('port')
            self.username = mqtt_settings.get('username')
            self.password = mqtt_settings.get('password')
            self.topic = mqtt_settings.get('topic')
            file.close
        except Exception as e:
            raise

    def consumer(self,q):
        while(True):
            name = threading.currentThread().getName()
            #print("{0}: All Conditions met, sending data".format(name))
            item = q.get()
            print(item)
            self.client.publish(self.topic,item,qos=0)
            q.task_done()


    def producer(self,q):
        # the main thread will put new items to the queue infinitely
        try:
            cmd = ['/home/pi/raspberry-gateway/database/./filter.sh']
            with Popen(cmd,stdout=PIPE,bufsize=1,universal_newlines=True) as p:
                for line in p.stdout:
                    line = line.strip()
                    deviceData = line.split(',')
                    name = threading.currentThread().getName()
                    beaconID = deviceData[1]
                    btMACaddress = deviceData[2]
                    rssi = deviceData[3]
                    payload = self.convertPayload(deviceData[4])
                    output = ','.join(deviceData)
                    #start_time = time.time()
                    if self.checkData(beaconID,output):
                        #print("{0}: adding beacon to cache".format(name))
                        #q.put(servercache[beaconID])
                        #print("---{0} seconds: ---".format(time.time() - start_time))
                        q.put(output)        
                        #print("KEY: {0}, VALUE: {1}".format(beaconID,self.cache[beaconID]))
            q.join()
        except OSError as e:
            print("Error Connecting:")

    def currentTime(self):
        curtime = datetime.now()
        return curtime.strftime('%Y-%m-%d %H:%M:%S')

    def convertPayload(self,data):
        line = data
        n = 2
        return [line[i:i+n] for i in range(0,len(line),n)]

    def intervalCheck(self,beaconID,difference,interval):
        #interval = interval * 60
        lastsent = difference - int(interval)
        #logging.debug("Last Sent = {0} ( {1} - {2} )".format(lastsent,difference,interval))
        if lastsent >= 0:
            return True
        else:
            return False

    def checkData(self,beaconID,output):
        start_time = time.time()
        if self.cache.get(beaconID):
            prev_time = datetime.strptime(self.cache[beaconID][0],'%Y-%m-%d %H:%M:%S')
            cur_time = datetime.strptime(self.currentTime(), '%Y-%m-%d %H:%M:%S')
            difference = cur_time - prev_time
            if self.intervalCheck(beaconID,difference.seconds,self.interval):
                self.cache[beaconID] = [self.currentTime(),output]
                #logging.debug("---{0} seconds: ---".format(time.time() - start_time))
                return True
            else:
                #logging.debug("FALSE---{0} seconds: BEACON {1} ---".format(time.time() - start_time,beaconID))
                return False
        else:
            #if beacon is not in the server cache add and return TRUE so send right away
            self.cache[beaconID] = [self.currentTime(),output]
            #logging.debug("---{0} seconds: ---".format(time.time() - start_time))
            return True

        
    def createConnection(self):
        try:
            self.client = mqtt.Client()
            #client.on_connect = on_connect
            #client.on_message = on_message
            #client.on_publish = on_publish
            self.client.username_pw_set(self.username,self.password)
            self.client.connect(self.host,int(self.port),60)
            q = queue.Queue()
            threads_num = 1  #create 20 threads
            for i in range(threads_num):
                t = threading.Thread(name = "Consumer Thread-"+str(i), target=self.consumer, args=(q,))
                t.start()
            t = threading.Thread(name = "Producer Thread", target=self.producer, args=(q,))
            t.start()
            q.join()
            print("All threads started")
            self.client.loop_forever()
        except Exception as e:
            raise
  


def main():
    test = deviceConfiguration()
    print(test.interval)
    print(test.host)
    print(test.port)
    print(test.username)
    print(test.password)
    print(test.topic)
    sleep(10)
    test.createConnection()
    
if __name__ == "__main__":
    main()
 
