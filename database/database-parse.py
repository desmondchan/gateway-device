from subprocess import Popen,PIPE, call
import sqlite3
from datetime import datetime, timedelta
import threading
import queue
import time

conn = sqlite3.connect('beacon-data.db')
interval = 1 #default value for interval

#create new database for beacons
def createDb():
    conn.execute("DROP TABLE IF EXISTS BEACON")
    conn.execute('''CREATE TABLE BEACON
                (ID TEXT PRIMARY KEY,
                INTERVAL INT,
                RSSI INT NOT NULL,
                IMPCOUNT TEXT NOT NULL,
                TEMP TEXT NOT NULL,
                LIGHT TEXT NOT NULL,
                CURRENTTIME TIMESTAMP NOT NULL,
                LASTSENT TIMESTAMP
                );''')
        
#get current time    
def currentTime():
    curtime = datetime.now()
    return curtime.strftime('%Y-%m-%d %H:%M:%S')

#checks interval and does not update if current time is < than prev time + interval
#return true and update database + send
def intervalCheck(difference,interval):
    interval = interval * 60
    lastsent = difference - interval
    if lastsent >= 0:
        print("Report is {0} seconds overdue, sending now".format(lastsent))
        return True
    else:
        print("Next report will be sent after {0} seconds".format(interval - difference))
        return False

#split the last part of parsed data into groups of 2
def convertPayload(data):
    line = data
    n = 2
    return [line[i:i+n] for i in range(0,len(line),n)]

def publish(output):
    return call(['mosquitto_pub','-d','-h','54.174.144.153','-p','61613','-u','admin','-P','password','-t','topics/bewhere_mqtt','-m', output])

def manageDB(conn,deviceData):
     beaconID = deviceData[1]
     btMACaddress = deviceData[2]
     rssi = deviceData[3]
     payload = convertPayload(deviceData[4])
     output = ','.join(deviceData)
     #check current database data to decide whether to send or not
     cur = conn.cursor()
     cur.execute("SELECT LASTSENT FROM BEACON WHERE ID = ?",(beaconID,))
     result = cur.fetchone()
     if result is None:
         print("Beacon {0} first reported at: {1}".format(beaconID,currentTime()))
         conn.execute("INSERT OR REPLACE INTO BEACON (ID,INTERVAL,RSSI,IMPCOUNT,TEMP,LIGHT,CURRENTTIME,LASTSENT) VALUES (?,?,?,?,?,?,?,?);",(beaconID,interval,rssi,payload[12],payload[13],payload[14],currentTime(),currentTime()));
         conn.commit()
         publish(output)
         #initial send here
     else:
         if result[0] is None:
             print("Last report sent from {0} was at {1}".format(beaconID,result[0]))
             conn.execute("UPDATE BEACON set LASTSENT = ? WHERE ID=?",(currentTime(),beaconID));
             conn.commit()
             #can probably remove this check if handled in first iteration
         else:
             print("Last report sent from {0} was at {1}".format(beaconID,result[0]))
             prev_time = datetime.strptime(result[0],'%Y-%m-%d %H:%M:%S')
             cur_time = datetime.strptime(currentTime(), '%Y-%m-%d %H:%M:%S')
             difference = cur_time - prev_time
             print("Last message was {0} seconds ago".format(difference.seconds))
             if intervalCheck(difference.seconds,interval):
                 conn.execute("INSERT OR REPLACE INTO BEACON (ID,INTERVAL,RSSI,IMPCOUNT,TEMP,LIGHT,CURRENTTIME,LASTSENT) VALUES (?,?,?,?,?,?,?,?);",(beaconID,interval,rssi,payload[12],payload[13],payload[14],currentTime(),currentTime() ));
                 conn.commit()
                 publish(output)
                 #send data here if conditions are met
     conn.execute("UPDATE BEACON set CURRENTTIME = ? WHERE ID=?",(currentTime(),beaconID));
     conn.commit()
    



#for each parsed line, spit into appropriate sections
#then store into a sqlite3 database w/ timestamp
def main():
    createDb()
    cmd = ['/home/pi/raspberry-gateway/database/./parser.sh','/home/pi/raspberry-gateway/database/config.ini']
    with Popen(cmd,stdout=PIPE,bufsize=1,universal_newlines=True) as p:
        for line in p.stdout:
            line = line.strip()
            deviceData = line.split(',')
            #do multi-threading or processing here
            manageDB(conn,deviceData)
           

           

            
if __name__ == "__main__":
    main()



