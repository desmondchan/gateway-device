from sqlalchemy import *
from sqlalchemy.orm import scoped_session,sessionmaker,relationship,backref
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine('sqlite:///database.sqlite3',convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=engine))
#engine = create_engine('sqlite://:memory:',connect_args={'check_same_thread':False},poolclass=StaticPool,echo=True)

Base = declarative_base()
Base.query = db.session.query_property()

class Beacon(Base):
    __tablename__ = 'beacons'
    id = Column(String,primary_key=True)
    interval = Column(Integer)
    rssi = Column(Integer)
    impcount = Column(String)
    temp = Column(String)
    light = Column(String)
    
