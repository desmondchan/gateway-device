from subprocess import Popen,PIPE, call
from datetime import datetime, timedelta
from cachetools import LRUCache

cache = LRUCache(maxsize=50)

#beaconID = 'test'
#cache[beaconID] = ['timestamp','data']
#print(cache[beaconID])
#print(cache[beaconID][1])
#print(cache)

#get current time    
def currentTime():
    curtime = datetime.now()
    return curtime.strftime('%Y-%m-%d %H:%M:%S')

def convertPayload(data):
    line = data
    n = 2
    return [line[i:i+n] for i in range(0,len(line),n)]

def manageData(deviceData):
    beaconID = deviceData[1]
    btMACaddress = deviceData[2]
    rssi = deviceData[3]
    payload = convertPayload(deviceData[4])
    output = ','.join(deviceData)
    if any(cache[beaconID])
    test = {beaconID : [currentTime(),"RSSI: {0}".format(rssi),"Temperature: {0}".format(payload[13]),"Light: {0}".format(payload[14])]}
    cache.update(test)
    #cache[beaconID] = [currentTime(),deviceData]
    #print(beaconID)
    #print(cache[beaconID])
    if beaconID == '000780B6AC64':
        print(cache[beaconID])
        

def main():
    cmd = ['/home/pi/raspberry-gateway/database/./parser.sh','/home/pi/raspberry-gateway/database/config.ini']
    with Popen(cmd,stdout=PIPE,bufsize=1,universal_newlines=True) as p:
        for line in p.stdout:
            line = line.strip()
            deviceData = line.split(',')
            manageData(deviceData)

if __name__ == "__main__":
    main()
import functools

@functools.lru_cache(maxsize=30)
def fib(num):
    if num < 2:
        return num
    else:
        return fib(num-1) + fib(num-2)


if __name__ == "__main__":
    print(list(fib(n) for n in range(16)))
