from subprocess import Popen,PIPE, call
from datetime import datetime, timedelta
import queue
import threading

cache = {}
servercache = {}
interval = 1

#get current time    
def currentTime():
    curtime = datetime.now()
    return curtime.strftime('%Y-%m-%d %H:%M:%S')

#splits the payload into groups of 2 to represent bytes
def convertPayload(data):
    line = data
    n = 2
    return [line[i:i+n] for i in range(0,len(line),n)]


def intervalCheck(beaconID,difference,interval):
    interval = interval * 60
    lastsent = difference - interval
    if lastsent >= 0:
        print("Device {0}:Report is {1} seconds overdue, sending now".format(beaconID,lastsent))
        return True
    else:
        #print("Device {0}: Next report will be sent after {1} seconds".format(beaconID,interval - difference))
        return False


#compare cache with servercache to determine sending of data to MQTT server
#add check for time interval and return
#if beacon is added AND time sent < interval DONT send so return FALSE
def checkData(beaconID,output):
    if beaconID in servercache:
        prev_time = datetime.strptime(servercache[beaconID][0],'%Y-%m-%d %H:%M:%S')
        cur_time = datetime.strptime(currentTime(), '%Y-%m-%d %H:%M:%S')
        difference = cur_time - prev_time
        if intervalCheck(beaconID,difference.seconds,interval):
            servercache[beaconID] = [currentTime(),output]
            return True
        else:
            return False
    else:
        #if beacon is not in the server cache add and return TRUE so send right away
        servercache[beaconID] = [currentTime(),output]
        return True

           
def consumer(q):
    while(True):
        name = threading.currentThread().getName()
        #print("{0}: All Conditions met, sending data".format(name))
        item = q.get()
        #print(item)
        q.task_done()


def producer(q):
    # the main thread will put new items to the queue infinitely
    cmd = ['/home/pi/raspberry-gateway/database/./parser.sh','/home/pi/raspberry-gateway/database/config.ini']
    with Popen(cmd,stdout=PIPE,bufsize=1,universal_newlines=True) as p:
        for line in p.stdout:
            line = line.strip()
            deviceData = line.split(',')
            name = threading.currentThread().getName()
            beaconID = deviceData[1]
            btMACaddress = deviceData[2]
            rssi = deviceData[3]
            payload = convertPayload(deviceData[4])
            output = ','.join(deviceData)
            if checkData(beaconID,output):
                #print("{0}: adding beacon to cache".format(name))
                q.put(servercache[beaconID])
            #cache[beaconID] = [currentTime(),output]
            #print("KEY: {0}, VALUE: {1}".format(beaconID,cache[beaconID]))
    q.join()
            
if __name__ == "__main__":
    q = queue.Queue()
    threads_num = 20  #create 10 threads
    for i in range(threads_num):
        t = threading.Thread(name = "Consumer Thread-"+str(i), target=consumer, args=(q,))
        t.start()

    t = threading.Thread(name = "Producer Thread", target=producer, args=(q,))
    t.start()
    q.join()
    print("All threads started")
    
