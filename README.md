# Orange Pi Gateway Device#
The Orange Pi Gateway Device is a replacement for the BLE Gateway which is currently being bought by a third-party. The Orange Pi will feature an Access Point mode and WiFi Router mode with an option for RSSI range and more features to be added later.

The code for the device has been written in both python and bash and should be portable to any Linux based device with a WiFi adapter and bluetooth capabilities. 

### Libraries ###
* **Flask** as the backend web framework
* **Flask-WTF** for form handling
* **paho-mqtt** for sending MQTT data
* **pyA20** for managing the GPIO
* **wifi** for managing connections
* **configparser** for managing the configuration file

### Features ###
* Access Point mode
* Python Flask Web server to display a web form to the IP 172.24.1.1
* Connects to WiFi through user submitted information
* Uses bash script to parse raw bluetooth data
* Sends parsed beacon data to back-end server via MQTT
* Uses Supervisor to monitor WiFi and Bluetooth to restart them if they fail


### User Configurable Settings ###
The current version of the device allows the users who connect to it to do the following:

* Scan for nearby networks to connect to
* Enter SSID/Passphrase for a router
* Enter RSSI filter from 0 - 100 (default to 100)

### In Progress ###
* Looking into setting up cron to automatically scan for network connection and then update
* Looking into adding Update button to web page to manually update
* Need to eventually switch from using shell script to filter with hcidump to a python based library in order to better track and contain errors within python