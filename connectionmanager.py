from wifi import Cell, Scheme
from wifi.exceptions import ConnectionError, InterfaceError
import subprocess
import configparser
import logging
import os
from time import sleep
logging.basicConfig(level=logging.DEBUG,format='%(asctime)s - %(levelname)s - %(message)s')
config = configparser.SafeConfigParser(allow_no_value=True, empty_lines_in_values=False)

"""
Module Name: connectionmanager.py
Author: Desmond Chan
Purpose: Activates WiFi using the information stored in /etc/network/interfaces

This module uses the wifi module found at https://github.com/rockymeza/wifi
If there is a saved scheme found for wlan0 under the name 'default' then it will
attempt to activate it up to 10 times and will return true if successful.
If unsuccessful, it will return false.

"""
wifi_config = '/home/pi/raspberry-gateway/config/webserver.conf'

def get_ssid():
    try:
        available_networks = set()
        check = 0
        while check < 3:
            network_list = list(Cell.all('wlan0'))
            for network in network_list:
                if network not in available_networks:
                    available_networks.add(str(network)[10:-1])
            check += 1
            sleep(5)

        logging.debug("Reading Wifi Config File ...")
        config.read(wifi_config)
        ssid = config.get('Wifi', 'ssid')
        if ssid not in available_networks:
            logging.debug("Saved SSID could not be found ... May be out of range")
            return False
        else:
            logging.debug("Saved SSID found ... Starting Reconnection attempts ...")
            return True
    except Exception as e:
        logging.debug("Error in network_test() : {0}".format(e))

def main():
    scheme = Scheme.find('wlan0', 'default')
    if scheme is not None:
        logging.debug("Saved Wifi scheme found, attempting to connect ...")
        attempt = 0
        wifidown = subprocess.Popen("sudo ifconfig wlan0 down", shell=True, stdout=subprocess.PIPE)
        sleep(1)
        wifiup = subprocess.Popen("sudo ifconfig wlan0 up", shell=True, stdout=subprocess.PIPE)

        if get_ssid() is False:
            logging.debug("Cannot find the SSID ... exiting")
            os._exit(0)

        while attempt < 10:
            try:
                sleep(5)
                logging.debug("Activating Wifi, attempt: {0}".format(attempt))
                scheme.activate()
                logging.debug("Wifi Activated!")
                return True
            except (ConnectionError, subprocess.CalledProcessError) as e:
                print("Attempt Failed ... Error ... {0}".format(e))
                attempt += 1
        logging.debug("Failed after 10 attempts ... rebooting device")
        os.system('reboot')
    else:
        logging.debug("No Scheme Found:")
        return False


if __name__ == '__main__':
    main()

