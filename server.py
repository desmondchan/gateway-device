from flask import Flask, render_template, request, jsonify, redirect, flash, url_for
from settings import SettingsForm
from wifi import Cell,Scheme
from wifi.exceptions import ConnectionError,InterfaceError
import subprocess
import configparser
import time
import os
import mqttmanager

app = Flask(__name__)
app.config['SECRET_KEY'] = "bewhere-development"

config = configparser.ConfigParser()

#connects to wifi and saves the scheme/deletes old one
def wifidata(ssidname,password):
    wifilist = []
    for i, var in enumerate(list(Cell.all('wlan0'))):
        wifilist.append(var.ssid)
    cellindex = wifilist.index(ssidname)
    celldata = list(Cell.all('wlan0'))[cellindex]
    scheme = Scheme.for_cell('wlan0','default',celldata,password)
    scheme.delete()
    scheme.save()
    return
    
    
@app.route('/')
def dashboard():
    return render_template('dashboard.html',title = "Dashboard")

@app.route('/reboot')
def reboot():
    time.sleep(5)
    return os.system('reboot')

#returns result of a wifi scan as json data to the requesting page
@app.route('/scan-wifi')
def scanning():
    ssidlist = []
    encryptiontype = []
    for i,item in enumerate(list(Cell.all('wlan0'))):
        if not item.ssid == "":
            ssidlist.append(item.ssid)
            encryptiontype.append(item.encryption_type)
    return jsonify(ssidname = ssidlist,
                   encryption = encryptiontype)

@app.route('/settings', methods=['GET','POST'])
def settings():
    form = SettingsForm(request.form)
    if request.method == 'POST' and form.validate():
        try:
            print("Reading File")
            file=open('/home/pi/raspberry-gateway/database/bewhere.conf','r+')
            config.read('/home/pi/raspberry-gateway/database/bewhere.conf')
            if not config.has_section("Wifi"):
                config.add_section("Wifi")
            config.set('Wifi','ssid',request.form['ssid'])
            config.set('Wifi','ssid_password',request.form['ssid_pw'])
            config.set('Wifi','rssi',request.form['rssi'])
            wifidata(ssidname=request.form['ssid'],password=request.form['ssid_pw'])
            config.write(file)
            flash('Config successfully updated, Please reboot the device now using the button below')
            file.close()
            return redirect(url_for('dashboard'))    
        except (IOError,InterfaceError) as e:
            print("Error: ", e)
        except ValueError:
            flash('Could not Find SSID with that name')
            
    return render_template('settings.html',title = "Settings",form=form)


def main():
    #if previous wifi-scheme is saved then use that data to connect to the internet and dont run the server
    #connect and start mqtt messaging w/ subprocess?        
    scheme = Scheme.find('wlan0','default')
    if scheme is not None:
        print("Saved Wifi Scheme found, auto-connecting....")
        conCounter = 0
        connection = True
        while connection and (conCounter < 10):
            try:
                print('Activating Scheme, attempt {0}'.format(conCounter))
                scheme.activate()
                connection = False
            except (ConnectionError,subprocess.CalledProcessError) as e:
                print('retrying connection',e)
                conCounter += 1
        try:
            file=open('/home/pi/raspberry-gateway/database/bewhere.conf','r')
            config.read('/home/pi/raspberry-gateway/database/bewhere.conf')
            #subprocess.call(['/home/pi/raspberry-gateway/database/./parser.sh', '/home/pi/raspberry-gateway/database/bewhere.conf'])
            mqttmanager.main()
        except (IOError, OSError) as e:
            print("config file does not exist or bad ssid/password combination\n", e)
            scheme.delete()
            #os.system('reboot')
    else:
        app.run(host='0.0.0.0',port=80,debug=True)

if __name__ == '__main__': main()



