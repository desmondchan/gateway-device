from flask_wtf import FlaskForm
from wtforms import StringField,PasswordField,IntegerField,SubmitField, SelectField
from wtforms import validators,ValidationError

"""
Module Name: settings.py 
Author: Desmond Chan
Purpose: Defines the fields for the web form in server.py 

Any changes to the web form such as new fields or removal of fields must be added here. 
Additionally handles simple validation using Flask WTF. 

"""


class SettingsForm(FlaskForm):
    ssid = StringField("SSID",[validators.Required("Please enter the name of the SSID you wish to connect to.")],render_kw={"placeholder":"Enter SSID Here"})
    ssid_pw = StringField("Passphrase",[validators.Required("Please enter your passphrase (Open networks are not currently supported.)")])
    submit = SubmitField("Submit")


class ApplicationForm(FlaskForm):
    host = StringField("Host")
    port = StringField("Port")
    topic = StringField("Topic")
    username = StringField("Username")
    password = StringField("Password")
    interval = IntegerField("Interval (seconds)")
    rssi = IntegerField("RSSI Range (0 to 100)",[validators.NumberRange(message="Please enter an RSSI number between 0 and 100",min=0,max=100)])
    gps = SelectField("GPS Enabled", choices=[('True', 'True'), ('False', 'False')])
    submit = SubmitField("Submit")
    

class LoginForm(FlaskForm):
    user_login = StringField("Username",[validators.Required("Please enter your username")])
    user_pass = PasswordField("Password",[validators.Required("Please enter your password")])
    submit = SubmitField("Login")

class SystemForm(FlaskForm):
    wifi_mac = StringField("WiFi MAC Address")
    bluetooth = StringField("Bluetooth MAC Address")
    serial = StringField("Serial Number")
    sender = StringField("Device ID (16 characters Max",[validators.Length(message="Please limit the Device ID to 16 characters or less",max=16)])
    submit = SubmitField("Submit")