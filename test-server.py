from server import app
import unittest
import json

class WebAppTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        self.app = app.test_client()
        self.app.testing = True

    def test_dashboard_page(self):
        result = self.app.get('/')
        self.assertEqual(result.status_code,200)

    def test_settings_get(self):
        result = self.app.get('/settings')
        self.assertEqual(result.status_code,200)

    def test_settings_post(self):
        result = self.app.post('/settings',data=dict(ssid="test",rssi='27'))
        self.assertIn('value="test"',str(result.data))
        self.assertIn('<input id="rssi" name="rssi" type="text" value="27">',str(result.data))
                               
    def test_scan(self):
        result = self.app.get('/scan-wifi',content_type='application/json')
        self.assertEqual(result.status_code,200)
        print("-----------------------Data From Scan WiFi--------------------------\n")
        print(str(result.get_data()))
        print("--------------------------------------------------------------------\n")





if __name__ == '__main__':
    unittest.main()
