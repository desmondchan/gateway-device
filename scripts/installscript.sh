#!/bin/bash
sudo apt-get update -y
#sudo apt-get install supervisor -y --force-yes
#sudo apt-get install inotify-tools -y --force-yes
sudo apt-get install hostapd -y --force-yes
sudo apt-get install dnsmasq -y --force-yes
sudo apt-get install python3-dev -y --force-yes
sudo apt-get install python3-pip -y --force-yes
sudo apt-get install bluez-hcidump -y --force-yes
sudo apt-get install gpsd gpsd-clients python-gpsd
sudo pip3 install -r stable-req.txt

#Replace Files
cp dnsmasq-config /etc/dnsmasq.conf
cp hostapd-default /etc/default/hostapd
cp interfaces-config /etc/network/interfaces
cp sysctl-config /etc/sysctl.conf
cp ftp_host ~/.ssh/known_hosts
#cp local-config /etc/rc.local

#File Permissions
sudo chmod u+x create-host.sh
sudo chmod u+x generate-mac.sh
sudo chmod u+x gpio/setup.py
sudo chmod u+x /home/pi/raspberry-gateway/supervisor-config/supervisor-config.sh
sudo chmod 600 /home/pi/raspberry-gateway/updates/orange-us-west-2.pem

#/bin/bash /home/pi/raspberry-gateway/supervisor-config/./supervisor-config.sh

#Setup gpio
python3 gpio/setup.py install

#Setup iptables
sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -A  FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"



sudo apt-get purge network-manager -y --force-yes

#disable gpsd
sudo systemctl stop gpsd.socket
sudo systemctl disable gpsd.socket
