#!/bin/bash

#generate random MAC address for the device
ifconfig wlan0 down
random="$(macchanger -A wlan0 | awk '/New MAC:/ {print $3;}')"
btmac="$(hcitool dev | awk '/hci0/ {print $2;}' | sed 's/://g')"
serial="$(cat /proc/cpuinfo | awk '/Serial/ {print $3;}')"

cat > /etc/modprobe.d/xradio_wlan.conf <<EOL

options xradio_wlan macaddr=${random^^}

EOL

cat /etc/modprobe.d/xradio_wlan.conf


cat > /etc/udev/rules.d/70-persistent-net.rules <<EOL


# Unknown net device (/devices/platform/sunxi-mmc.1/mmc_host/mmc1/mmc1:0001/mmc1:0001:1/net/wlan0) (xradio_wlan)
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="${random}", ATTR{dev_id}=="0x0", ATTR{type}=="1", KERNEL=="wlan*", NAME="wlan0"

EOL


cat /etc/udev/rules.d/70-persistent-net.rules

ifconfig wlan0 up

macaddr="$(ifconfig wlan0 | awk '/HWaddr/ {print $5;}' | sed 's/://g')"
cat > /etc/hostapd/hostapd.conf <<EOL

interface=wlan0
driver=nl80211
ssid=Bewhere-${macaddr^^}
hw_mode=g
channel=6
ieee80211n=1
wmm_enabled=1
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=bewhereconnect
wpa_key_mgmt=WPA-PSK
#wpa_pairwise=TKIP
rsn_pairwise=CCMP

EOL

cat /etc/hostapd/hostapd.conf

cat > /home/orange-gateway/config/deviceinfo.conf <<EOL

[System]
wifi_mac = ${macaddr^^}
bluetooth = $btmac
serial =
sender = ${macaddr^^}

EOL

cat /home/orange-gateway/config/deviceinfo.conf