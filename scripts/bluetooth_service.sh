#!/bin/bash

sudo hcitool lescan --duplicates
if [ $? -ne 0 ]; then
	echo "$? Bluetooth Error ... Restarting ..."
	sudo systemctl daemon-reload
	sudo service bluetooth restart
	sudo hciconfig hci0 up
	echo "Bluetooth Restarted"
fi

