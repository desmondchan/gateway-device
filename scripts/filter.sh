#!/bin/bash

config=/home/pi/raspberry-gateway/config/bewhere.conf
whitelist=/home/pi/raspberry-gateway/config/whitelist.txt

bewhereID1="77"
bewhereID2="02"
#get MAC Address for wlan0 of the raspberry pi device
mac="$(ifconfig wlan0 | awk '/^[a-z]/ {iface=$1;mac=$NF; next } /inet addr:/ {print mac}' | sed 's/://g')" 

#Bluetooth MAC address for raspberry pi device
btmac="$(hcitool dev | awk '/hci0/ {print $2;}' | sed 's/://g')"


#parses each hcidump as one line
#only retreives raw data with 1. FF 77 in the payload
#interval="$(grep -F -m 1 'interval' $1 | awk -F '= ' '{print $2}')" 

sudo hcidump -R | grep -A2 --no-group-separator '04 3E 2B .. .. .. .. .. .. .. .. .. .. .. .. .. .. 1. FF 77' | sed -u 's/[>]//g' | paste -d " " - - - | while read -r line ; do
	
	IFS=' ' read -r -a array <<< "$line"
	
	beaconMAC="${array[12]}${array[11]}${array[10]}${array[9]}${array[8]}${array[7]}"
	if [ -s $whitelist ]; then
		if ! grep -q ${beaconMAC} "$whitelist"; then
			continue
		fi
	fi
	deviceID1="${array[19]}"
	deviceID2="${array[20]}"
	
	if ! [ "$deviceID1" == "$bewhereID1" ] && [ "$deviceID2" == "$bewhereID2" ]; then 
		printf "device ID does not match BeWhere ID, continuing....\n"
		continue;
	fi

	#convert last element of payload into rssi number
	#skip if less than the filter
	rssi="$((16#${array[45]} - 256))"
	rssifilter="$(grep -F -m 1 'rssi' $config | awk -F '= ' '{print $2}')" 
	convert="$((rssifilter * -1))"
	if (("$rssi" < "$convert")); then
		#echo  "${rssi} is less than ${convert}, continuing"
		continue;
	fi

	#first half of output
	output="\$GPRP,${beaconMAC^^},${mac^^},${rssi},"
	
	#loops through the relevant array values and concatenates it to the final output
	for i in `seq 14 45`
	do
		#echo "array[$i] ${array[$i]}"
		output="$output${array[$i]}"
		((i++))
	done

	#printf "${output}\n"
	STR="${output}"
	echo $STR
	#echo "$(mosquitto_pub -d -h 54.174.144.153 -p 61613 -u admin -P password -t topics/bewhere_mqtt -m ${output}	)"

done
