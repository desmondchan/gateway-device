#!/bin/bash

macaddr="$(ifconfig wlan0 | awk '/^[a-z]/ {iface=$1;mac=$NF; next} /inet addr:/ {print mac}' | sed 's/://g')"

cat > /etc/hostapd/hostapd.conf <<EOL

interface=wlan0
driver=nl80211
ssid=Bewhere-${macaddr^^}
hw_mode=g
channel=6
ieee80211n=1
wmm_enabled=1
ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=raspberry
wpa_key_mgmt=WPA-PSK
#wpa_pairwise=TKIP
rsn_pairwise=CCMP

EOL

cat /etc/hostapd/hostapd.conf
