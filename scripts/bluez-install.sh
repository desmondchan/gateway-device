#!/bin/bash

cd ~
wget http://www.kernel.org/pub/linux/bluetooth/bluez-5.44.tar.xz
tar xvf bluez-5.44.tar.xz
cd bluez-5.44
sudo apt-get install -y libusb-dev libdbus-1-dev libglib2.0-dev libudev-dev libical-dev libreadline-dev
sudo ~/bluez-5.44/configure
make -C ~/bluez-5.44
make install -C ~/bluez-5.44
systemctl status bluetooth
sudo systemctl start bluetooth
sudo systemctl daemon-reload
sudo systemctl restart bluetooth
systemctl status bluetooth
rm -rf ~/bluez-5.44
rm -rf ~/bluez-5.44.tar.xz