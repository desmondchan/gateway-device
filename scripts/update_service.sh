#!/bin/bash
time=$(date)

sleep 60

wget -q --spider http://bewhere.com
if [ $? -eq 0 ]; then
	mv /home/pi/raspberry-gateway/updates/deviceupdate.sh /home/pi/raspberry-gateway/updates/oldupdate.sh
	scp -i /home/pi/raspberry-gateway/updates/orange-us-west-2.pem ubuntu@ftp-orange.bewhere.com:/home/ubuntu/updates/deviceupdate.sh /home/pi/raspberry-gateway/updates/
	if cmp -s "/home/pi/raspberry-gateway/updates/oldupdate.sh" "/home/pi/raspberry-gateway/updates/deviceupdate.sh"; then
		echo "Checked for updates at $time .... no changes" 
	else
		echo "File Changed ... "
		sudo chmod u+x /home/pi/raspberry-gateway/updates/./deviceupdate.sh
		sudo /home/pi/raspberry-gateway/updates/./deviceupdate.sh
	fi
else
	echo "Checked for updates at $time .... No Internet Connection" 
fi

sleep 3600
