#!/bin/bash
btmac="$(hcitool dev | awk '/hci0/ {print $2;}' | sed 's/://g')"
macaddr="$(ifconfig wlan0 | awk '/^[a-z]/ {iface=$1;mac=$NF; next} /inet addr:/ {print mac}' | sed 's/://g')"

cat > /home/pi/orange-gateway/config/deviceinfo.conf <<EOL
[System]
wifi_mac = ${macaddr^^}
bluetooth = $btmac
serial =
sender = ${macaddr^^}

EOL

cat /home/pi/orange-gateway/config/deviceinfo.conf