#!/bin/bash

#get MAC Address for wlan0 of the orange pi device
mac1="MAC: "
mac2="$(ifconfig wlan0 | awk '/^[a-z]/ {iface=$1;mac=$NF; next } /inet addr:/ {print mac}' | sed 's/://g')"
mac="$mac1 $mac2" 

#Bluetooth MAC address for orange pi device
btmac1="Bluetooth: "
btmac2="$(hcitool dev | awk '/hci0/ {print $2;}' | sed 's/://g')"
btmac="$btmac1 $btmac2"


serial1="Serial: "
serial2="$(cat /home/orange-gateway/config/deviceinfo.conf | awk '/Serial/ {print $3;}')"
serial="$serial1 $serial2"

echo ${mac^^}
echo $btmac
echo $serial

cat > /home/orange-gateway/config/deviceinfo.conf <<EOL

[System]
wifi_mac = ${mac2^^}
bluetooth = $btmac2
serial =  $serial2
sender = ${mac2^^}

EOL

cat /home/orange-gateway/config/deviceinfo.conf