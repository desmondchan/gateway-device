import os
import subprocess
from wifi import Scheme
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(23,GPIO.IN,pull_up_down = GPIO.PUD_DOWN)

try:
    while True:
        GPIO.wait_for_edge(23,GPIO.RISING)
        print("Reboot Button Pressed")
        scheme = Scheme.find('wlan0','default')
        if scheme is not None:
            scheme.delete()
        os.system('reboot')
        GPIO.wait_for_edge(23,GPIO.FALLING)
except Exception as e:
    print("Error: " + str(e))
    GPIO.cleanup()
