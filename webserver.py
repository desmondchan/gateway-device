from flask import Flask, render_template, request, jsonify, redirect, flash, url_for
import flask_login
from settings import SettingsForm, LoginForm, ApplicationForm, SystemForm
from wifi import Cell, Scheme
from wifi.exceptions import ConnectionError, InterfaceError
import subprocess
import configparser
import datetime
import time
import os
import logging
import userlogin

app = Flask(__name__)
app.config['SECRET_KEY'] = "bewhere-development"


wifi_config = '/home/pi/raspberry-gateway/config/webserver.conf'
application_config = '/home/pi/raspberry-gateway/config/bewhere.conf'
device_config = '/home/pi/raspberry-gateway/config/deviceinfo.conf'
generate_info = '/home/pi/raspberry-gateway/scripts/generate-mac.sh'
refresh_info = '/home/pi/raspberry-gateway/scripts/deviceinfo.sh'

#Development path
#wifi_config = '/Users/development/orange-gateway/config/webserver.conf'
#application_config = '/Users/development/orange-gateway/config/bewhere.conf'
#device_config = '/Users/development/orange-gateway/config/deviceinfo.conf'




login_manager = flask_login.LoginManager()
login_manager.init_app(app)

"""
Module Name: server.py
Author: Desmond Chan
Purpose: Manages the web server displayed to the user when no WiFi scheme is available.

This manages the web application displayed to the user and allows them to submit configuration
data such as SSID/Passphrase and RSSI on a web form with fields defined in settings.py.
The saved configuration data will be stored in bewhere.conf.

If the WiFi connection is successful then a device update request will be written to updatelog.txt
this request will trigger an update script which will check the ftp server and update if necessary.


"""

@login_manager.user_loader
def user_loader(username):
    if username != 'admin':
        return
    user = userlogin.User()
    user.id = username
    return user

@login_manager.unauthorized_handler
def unauthorized_handler():
    flash("Please login to view this page")
    return redirect(url_for('login'))


"""
wifidata()
connects to wifi and saves the scheme/deletes old one in /etc/network/interfaces
to overcome the orange pi's inability to scan in AP mode, a virtual interface wlan1
is used to scan for networks, which are saved in wlan0
"""

def wifidata(ssidname, password):
    wifilist = []
    for i, var in enumerate(list(Cell.all('wlan0'))):
        wifilist.append(var.ssid)
    cellindex = wifilist.index(ssidname)
    celldata = list(Cell.all('wlan0'))[cellindex]
    scheme = Scheme.for_cell('wlan0', 'default', celldata, password)
    scheme.delete()
    scheme.save()
    return


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class
        username = request.form['user_login']
        if userlogin.validate_password(userlogin.get_password(), request.form['user_pass']):
            user = userlogin.User()
            user.id = username
            flask_login.login_user(user)
            return redirect(url_for('dashboard'))
    return render_template('login.html', form=form, title="Login")

@app.route("/logout")
def logout():
    flask_login.logout_user()
    return redirect(url_for('login'))


@app.route('/')
@flask_login.login_required
def dashboard():
    return render_template('dashboard.html', title="Home")

@app.route('/reboot')
@flask_login.login_required
def reboot():
    time.sleep(5)
    flask_login.logout_user()
    return os.system('reboot')

#returns result of a wifi scan as json data to the requesting page
@app.route('/scan-wifi')
def scanning():
    ssidlist = []
    encryptiontype = []
    for i, item in enumerate(list(Cell.all('wlan0'))):
        if not item.ssid == "":
            ssidlist.append(item.ssid)
            encryptiontype.append(item.encryption_type)
    return jsonify(ssidname=ssidlist, encryption=encryptiontype)


@app.route('/system', methods=['GET', 'POST'])
@flask_login.login_required
def system():
    sysconfig = configparser.SafeConfigParser(allow_no_value=True, empty_lines_in_values=False)
    form = SystemForm(request.form)
    generate = False
    if request.method == 'POST' and form.validate():
        try:
            sysconfig.read(device_config)
            sysconfig.set('System', 'serial', request.form['serial'])
            sysconfig.set('System', 'sender', request.form['sender'])
            flash("System Settings successfully updated")
            with open(device_config, 'w') as sys_file:
                sysconfig.write(sys_file)
            return redirect(url_for('dashboard'))

        except(IOError, InterfaceError, ValueError) as e:
            logging.debug("Error in System page with post method: {0}".format(e))
    try:
        sysconfig.read(device_config)
        form.wifi_mac.data = sysconfig.get('System', 'wifi_mac')
        form.bluetooth.data = sysconfig.get('System', 'bluetooth')
        form.serial.data = sysconfig.get('System', 'serial')
        form.sender.data = sysconfig.get('System', 'sender')
        if not (form.wifi_mac.data and form.bluetooth.data):
            generate = True
    except(IOError, InterfaceError, ValueError) as e:
        logging.debug("Error in System page: {0}".format(e))
    return render_template('system.html',
                           title="System",
                           generate=generate,
                           form=form)

@app.route('/generate-info')
@flask_login.login_required
def generate():
    os.system(generate_info)
    return os.system('reboot')

@app.route('/refresh-info')
@flask_login.login_required
def refresh():
    os.system(refresh_info)
    return redirect(url_for('system'))

@app.route('/application', methods=['GET', 'POST'])
@flask_login.login_required
def application():
    appconfig = configparser.SafeConfigParser(allow_no_value=True, empty_lines_in_values=False)
    form = ApplicationForm(request.form)
    if request.method == 'POST' and form.validate():
        try:
            logging.debug("Reading Application Config File ...")
            appconfig.read(application_config)
            appconfig.set('Misc', 'interval', request.form['interval'])
            appconfig.set('Misc', 'gps', request.form['gps'])
            appconfig.set('Misc', 'rssi', request.form['rssi'])
            flash("Application Settings successfully updated")
            with open(application_config, 'w') as app_file:
                appconfig.write(app_file)
            return redirect(url_for('dashboard'))

        except(IOError, InterfaceError, ValueError) as e:
            logging.debug("Error in Application page: {0}".format(e))
    try:
        appconfig.read(application_config)
        form.host.data = appconfig.get('MQTT', 'host')
        form.port.data = appconfig.get('MQTT', 'port')
        form.username.data = appconfig.get('MQTT', 'username')
        form.password.data = appconfig.get('MQTT', 'password')
        form.topic.data = appconfig.get('MQTT', 'topic')
        form.interval.data = appconfig.get('Misc', 'interval')
        form.gps.data = appconfig.get('Misc', 'gps')
        form.rssi.data = appconfig.get('Misc', 'rssi')
    except(IOError, InterfaceError, ValueError) as e:
        logging.debug("Error in Application page: {0}".format(e))
    return render_template('application.html',
                           title="Application",
                           form=form)


@app.route('/wifi', methods=['GET', 'POST'])
@flask_login.login_required
def settings():
    wificonfig = configparser.SafeConfigParser(allow_no_value=True, empty_lines_in_values=False)
    form = SettingsForm(request.form)
    if request.method == 'POST' and form.validate():
        try:
            print("Reading File")
            wificonfig.read(wifi_config)
            if not wificonfig.has_section("Wifi"):
                wificonfig.add_section("Wifi")
            wificonfig.set('Wifi', 'ssid', request.form['ssid'].strip('\n'))
            wificonfig.set('Wifi', 'ssid_password', request.form['ssid_pw'].strip('\n'))
            wifidata(ssidname=request.form['ssid'], password=request.form['ssid_pw'])
            with open(wifi_config, 'w') as wifi_file:
                wificonfig.write(wifi_file)
            flash('Config successfully updated, Please reboot the device now using the button below')
            return redirect(url_for('dashboard'))
        except (IOError, InterfaceError) as e:
            print("Error: ", e)
        except ValueError:
            flash('Error getting network information ... Please try again')

    return render_template('wifi.html', title="WiFi", form=form)


def main():
    try:
        app.run(host='0.0.0.0', port=80, debug=True)
    except Exception as e:
        logging.debug("Error in webserver.py: {0}".format(e))

if __name__ == '__main__': main()



